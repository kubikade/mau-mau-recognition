﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace hw07_maumau.Controls
{
    public class CardImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //var image = new Image();
            var filePath = "";
            if (value != null)
            {
                filePath = Path.Combine(Directory.GetParent(
                    Directory.GetCurrentDirectory()).Parent.FullName, 
                    "Resources/Cards", value.ToString()) + ".jpg";

                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(filePath, UriKind.Absolute);
                bitmap.EndInit();

                return bitmap;
            }
            return filePath;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
