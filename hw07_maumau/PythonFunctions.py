def hello(name):
	print "Hello " + name + "! Welcome to IronPython!"
	return

def add(x, y):
	print "%i + %i = %i" % (x, y, (x + y))
	return

def multiply(x, y):
	print "%i * %i = %i" % (x, y, (x * y))
	return



name = 'Chris'
age = 21