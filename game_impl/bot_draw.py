import csv
import os

# Importuji jednotlive slozky implementace hry
import mau_mau_impl as mmi
import draw_card as dr
import card_recognition.card_recognition as cr

# current_dir_path = os.path.abspath(os.path.join(os.path.abspath(__file__)))
top_card_path = os.path.abspath(os.path.join("../resources/play/top_card.tsv"))


def save_drawn(drawn_card, bot_name):
    player_path = mmi.get_abs_path_from_project_directory("resources/play")
    new_path = player_path + "/" + str(bot_name) + ".tsv"
    new_card = dr.draw_card_from_deck()
    with open(mmi.resource_path(new_path), 'r'):
        player_cards = mmi.get_cards(new_path)
    # with open(resource_path(new_path), 'wt') as hand_file:        # old
    with open(mmi.resource_path(new_path), 'wb') as hand_file:
        player_cards.append(drawn_card)
        tsv_writer = csv.writer(hand_file, delimiter='\t')
        tsv_writer.writerows(player_cards)
    return


if __name__ == "__main__":
    card_pair = cr.recognize_drawn_card()[0]
    drawn_card = []
    for i in card_pair:
        drawn_card.append(i)
    drawn_card = [str(drawn_card[0] + '_' + drawn_card[1])]
    bot_name = "bot"
    save_drawn(drawn_card, bot_name)
