'''
In this script is implemented function to make a move and is mostly a copy of the play_one_turn.py script
This is called from the C# project when robot is trying to make a move.
This script accepts only 1 argument setting current playing number and checking whether bot is playing or not
This script also implements basic logic of the game as play_one_turn.py script does.
'''

import csv
import os
import random
import sys

# Importuji jednotlive slozky implementace hry
import mau_mau_impl as mmi
import card_recognition.card_recognition as cr



current_dir_path = os.path.abspath(os.path.join(os.path.abspath(__file__)))
# top_card_path = os.path.abspath(os.path.join("../resources/play/top_card.tsv"))

top_card_path = os.path.abspath(os.path.join(current_dir_path, "../../resources/play/top_card.tsv"))


def play_one_turn_bot(bot_name, top, card=None, new_color=None):
    # print "chce hrat kartou " + card
    # hrajeme bez prebijeni
    player_path = mmi.get_abs_path_from_project_directory("resources/play")
    new_path = player_path + "/" + str(bot_name) + ".tsv"
    crd = top[0]
    values_top = crd.split("_")  # list cislo, barva vrchni karty
    topdeck = top  # list karta, cislo
    # prvni vec je karta string a druha vec je cislo 0 nebo 1
    used_card = "1"
    if len(topdeck) > 1:
        used_card = str(topdeck[1])  # to je to cislo
    if values_top[0] == "7" and used_card == "0":
        # draw 2 cards
        # print "Beru dve"
        mmi.change_draw(2)
        # with open(mmi.resource_path(new_path), 'r'):
        #     player_cards = mmi.get_cards(new_path)
        # with open(mmi.resource_path(new_path), 'wb') as hand_file:
        #     tsv_writer = csv.writer(hand_file, delimiter='\t')
        #     new_card = dr.draw_card_from_deck()
        #     player_cards.append([new_card])
        #     new_card = dr.draw_card_from_deck()
        #     player_cards.append([new_card])
        #     tsv_writer.writerows(player_cards)
        #     mmi.change_topdeck(top, 1)
        return
        # mmi.change_topdeck(top[0], 1)
        # return
    elif values_top[0] == "A" and used_card == "0":
        # print "Stojim"
        mmi.change_topdeck(top[0], 1)
        return
    else:
        if card is None:
            # print "Zadnou kartu nehraju - lizu kartu"
            mmi.change_draw(1)
            return
        else:
            values_card = card.split("_")
            if values_card[0] == "Q":
                # print "Hraju menice"
                # je to menic
                new_color = random.choice(["A", "B", "L", "H"])
                with open(mmi.resource_path(new_path), 'r'):
                    list = mmi.get_cards(new_path)
                    list.remove([card])
                # with open(resource_path(new_path), 'wt') as hand_file:        # old
                with open(mmi.resource_path(new_path), 'wb') as hand_file:
                    tsv_writer = csv.writer(hand_file, delimiter='\t')
                    tsv_writer.writerows(list)
                values_card[1] = new_color
                str_card = str(values_card[0]) + "_" + str(values_card[1])
                mmi.change_topdeck(str_card, 0)
            else:
                # neni to menic
                if values_top[0] == values_card[0] or values_top[1] == values_card[1]:
                    # print "Hraju kartu"
                    if values_card[0] == "7" or values_card[0] == "A":
                        mmi.change_topdeck(card, 0)
                    else:
                        mmi.change_topdeck(card, 0)
                    with open(mmi.resource_path(new_path), 'r') as hand_file:
                        list = mmi.get_cards(new_path)
                        list.remove([card])
                    with open(mmi.resource_path(new_path), 'wb') as hand_file:
                        tsv_writer = csv.writer(hand_file, delimiter='\t')
                        tsv_writer.writerows(list)
                else:
                    # print "Hraju nevalidni kartou"
                    return
    return


def main():
    type = None
    bot_name = "player" + str(mmi.BOT_NUMBER)
    card_pair = cr.recognize_topdeck()[0]
    is_valid = top = str(mmi.get_cards(top_card_path)[0][1])
    # print(is_valid)
    top_card = []
    for i in card_pair:
        top_card.append(i)
    # print(top_card)
    top_card = [str(top_card[0] + '_' + top_card[1]), is_valid]
    # print(top_card)
    chosen_card = mmi.bot_choose(top_card, bot_name)
    if chosen_card is not None:
        chosen_card = chosen_card[0]
    print(top_card)
    print(chosen_card)
    play_one_turn_bot(bot_name, top_card, chosen_card, type)

if __name__ == "__main__":
    player_number = None
    if len(sys.argv) == 2:
        player_number = sys.argv[1]
    if int(player_number) == int(mmi.BOT_NUMBER):
        print "True"      # oznamuju do terminalu, ze hraje bot
        main()
